from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():

    ld = LaunchDescription()

    node = Node(package = "tf2_ros", 
                executable = "static_transform_publisher",
                arguments = ["0.000", "0.059", "-0.090", "-1.570", "0.022", "-1.570", "lidar", "oakd"])
    # xyz ypr

    ld.add_action(node)

    return ld
