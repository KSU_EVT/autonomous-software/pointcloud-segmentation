import os

from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():
   config = os.path.join(
      get_package_share_directory('pointcloud_segmentation'),
      'config',
      'cone_detect_node.yaml'
   )

   return LaunchDescription([
      Node(
         package='pointcloud_segmentation',
         executable='cone_detect_node',
         namespace='',
         name='cone_detect',
         parameters=[config]
      ),
      Node(
         package = "tf2_ros", 
         executable = "static_transform_publisher",
         # xyz ypr
         arguments = ["0.000", "0.059", "-0.090", "-1.570", "0.022", "-1.570", "lidar", "oakd"]
      )
   ])
