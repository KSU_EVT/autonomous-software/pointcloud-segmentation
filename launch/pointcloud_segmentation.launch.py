
from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription
from launch_ros.actions import Node

def generate_launch_description():

    ld = LaunchDescription()
    ouster_driver = Node(package='simple_ouster_driver',
                         executable='simple_ouster_driver_node',
                         parameters=[
                             {'use_ros_time':False}
                         ])
    ld.add_action(ouster_driver)
    
    detection_node = Node(package='oakd_detection',
                          executable='detection_node')


    ld.add_action(detection_node)
    node = Node(package = 'pointcloud_segmentation', 
                executable = 'mask_cull_node',
                remappings=[
            ('/camera/seg', '/camera/box_seg'),
        ]
        )

    ld.add_action(node)

    tf_node = Node(package = "tf2_ros", 
                executable = "static_transform_publisher",
                arguments = ["0.03", "0.10", "-0.07", "-1.57", "0.0", "-1.56", "lidar", "oakd"])

    ld.add_action(tf_node)

    return ld
