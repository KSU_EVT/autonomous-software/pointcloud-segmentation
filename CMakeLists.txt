cmake_minimum_required(VERSION 3.8)

project(pointcloud_segmentation)

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()

set(CMAKE_C_FLAGS_DEBUG "-O0 -g")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG}")
set(CMAKE_C_FLAGS_RELEASE "-O3 -DNDEBUG")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE}")


# find dependencies
find_package(ament_cmake REQUIRED)
find_package(ament_cmake_ros REQUIRED)
find_package(rclcpp REQUIRED)
find_package(rclcpp_components REQUIRED)
find_package(sensor_msgs REQUIRED)
find_package(pcl_conversions REQUIRED)
find_package(pcl_ros REQUIRED)
find_package(PCL 1.9 REQUIRED COMPONENTS common io)
find_package(tf2 REQUIRED)
find_package(tf2_ros REQUIRED)
find_package(tf2_geometry_msgs REQUIRED)
find_package(tf2_sensor_msgs REQUIRED)
find_package(Eigen3 REQUIRED)
find_package(tf2_eigen REQUIRED)
find_package(std_msgs REQUIRED)
find_package(geometry_msgs REQUIRED)
find_package(voltron_msgs REQUIRED)
find_package(message_filters REQUIRED)
find_package(image_geometry 3.4.0 REQUIRED) # humble is too old
find_package(OpenCV REQUIRED)

add_library(pointcloud_segmentation
  src/color_point_cloud_node.cpp
  src/mask_cull_node.cpp
  src/cone_detect_node.cpp)
target_compile_features(pointcloud_segmentation PUBLIC c_std_99 cxx_std_20)  # Require C99 and C++20
ament_target_dependencies(pointcloud_segmentation
  PUBLIC
    rclcpp
    rclcpp_components
    sensor_msgs
    voltron_msgs
    geometry_msgs
    tf2
    tf2_ros
    tf2_sensor_msgs
    tf2_eigen
    message_filters
    image_geometry
    pcl_conversions
    pcl_ros
  )
target_include_directories(pointcloud_segmentation PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  $<INSTALL_INTERFACE:include>
  ${OpenCV_INCLUDE_DIRS})
target_link_libraries(pointcloud_segmentation PUBLIC ${OpenCV_LIBS})
install(
  DIRECTORY include/
  DESTINATION include
)
install(
  TARGETS pointcloud_segmentation
  EXPORT export_${PROJECT_NAME}
  ARCHIVE DESTINATION lib
  LIBRARY DESTINATION lib
  RUNTIME DESTINATION bin
)


add_executable(color_point_cloud_node src/color_point_cloud_node_main.cpp)
target_compile_features(color_point_cloud_node PUBLIC c_std_99 cxx_std_20)  # Require C99 and C++20
target_link_libraries(color_point_cloud_node pointcloud_segmentation)
install(TARGETS color_point_cloud_node
  DESTINATION lib/${PROJECT_NAME})

add_executable(mask_cull_node src/mask_cull_node_main.cpp)
target_compile_features(mask_cull_node PUBLIC c_std_99 cxx_std_20)  # Require C99 and C++20
target_link_libraries(mask_cull_node pointcloud_segmentation)
install(TARGETS mask_cull_node
  DESTINATION lib/${PROJECT_NAME})

add_executable(cone_detect_node src/cone_detect_node_main.cpp)
target_compile_features(cone_detect_node PUBLIC c_std_99 cxx_std_20)  # Require C99 and C++20
target_link_libraries(cone_detect_node pointcloud_segmentation)
install(TARGETS cone_detect_node
  DESTINATION lib/${PROJECT_NAME})

if(BUILD_TESTING)
  find_package(ament_lint_auto REQUIRED)
  # the following line skips the linter which checks for copyrights
  # comment the line when a copyright and license is added to all source files
  set(ament_cmake_copyright_FOUND TRUE)
  # the following line skips cpplint (only works in a git repo)
  # comment the line when this package is in a git repo and when
  # a copyright and license is added to all source files
  set(ament_cmake_cpplint_FOUND TRUE)
  ament_lint_auto_find_test_dependencies()
endif()

# install launch files
install(DIRECTORY
  launch
  DESTINATION share/${PROJECT_NAME}/
)
# install params files 
install(DIRECTORY
  config
  DESTINATION share/${PROJECT_NAME}/
)

# Install Python modules
ament_python_install_package(${PROJECT_NAME})
# Install Python executables
install(PROGRAMS
  scripts/mask_image_pub.py
  DESTINATION lib/${PROJECT_NAME}
)

ament_export_include_directories(
  include
)
ament_export_libraries(
  pointcloud_segmentation
)
ament_export_targets(
  export_${PROJECT_NAME}
)

ament_package()
