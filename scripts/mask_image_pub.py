#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Image, CameraInfo
from cv_bridge import CvBridge
import cv2 
import numpy as np


class TestMaskImagePub(Node):
    def __init__(self):
        super().__init__('test_mask_image_pub')
        self.publisher_ = self.create_publisher(Image, '/camera/seg', 10)
        self.camera_info_pub_ = self.create_publisher(CameraInfo, '/camera/camera_info', 10)

        self.camera_info_msg_ = CameraInfo()
        self.camera_info_msg_.header.frame_id = 'oakd';
        self.camera_info_msg_.width = 1920
        self.camera_info_msg_.height = 1080
        self.camera_info_msg_.distortion_model = "plumb_bob"
        self.camera_info_msg_.d = [0.114441, -0.189338, -0.000225, -0.002398, 0.000000]
        self.camera_info_msg_.k = [1559.490292, 0.000000, 934.650639,
                                0.000000, 1562.882223, 534.719406,
                                0.000000, 0.000000, 1.000000]
        self.camera_info_msg_.r = [1.000000, 0.000000, 0.000000,
                                0.000000, 1.000000, 0.000000,
                                0.000000, 0.000000, 1.000000]
        self.camera_info_msg_.p = [1583.767822, 0.000000, 929.517859, 0.000000,
                                0.000000, 1591.692017, 534.035444, 0.000000,
                                0.000000, 0.000000, 1.000000, 0.000000]


        timer_period = 0.05  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)
        self.br = CvBridge()

    def timer_callback(self):
        # Define image size and number of stripes
        img_size = (1920, 1080)
        num_stripes = 10

        # Create a black image with the desired size and 3 color channels
        img = np.zeros((img_size[1], img_size[0], 3), dtype=np.uint8)
        
        # Calculate the width of each stripe
        stripe_width = img_size[0] // num_stripes
        
        # Loop through the stripes and fill them with alternating colors
        for i in range(num_stripes):
            if i % 2 == 0:
                img[:, i*stripe_width:(i+1)*stripe_width] = (0, 0, 255) # red stripe
            else:
                img[:, i*stripe_width:(i+1)*stripe_width] = (0, 0, 0) # black stripe

        stamp = self.get_clock().now().to_msg()

        msg = self.br.cv2_to_imgmsg(img)
        msg.header.stamp = stamp
        msg.header.frame_id = 'oakd'
        msg.encoding = 'bgr8'
        self.publisher_.publish(msg)

        self.camera_info_msg_.header.stamp = stamp
        self.camera_info_pub_.publish(self.camera_info_msg_)

        self.get_logger().info('Published seg + info')


def main(args=None):
    rclpy.init(args=args)

    test_mask_image_pub = TestMaskImagePub()

    rclpy.spin(test_mask_image_pub)

    # Destroy the node explicitly
    test_mask_image_pub.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
