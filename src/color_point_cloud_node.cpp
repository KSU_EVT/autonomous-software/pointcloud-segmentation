#include "pointcloud_segmentation/color_point_cloud_node.hpp"
#include <tf2/convert.h>

#include <chrono>
#include <memory>
#include <functional>
#include <rclcpp/qos.hpp>
#include <rclcpp/subscription.hpp>
#include <geometry_msgs/msg/transform_stamped.hpp>
#include <sensor_msgs/point_cloud2_iterator.hpp>
#include <tf2_sensor_msgs/tf2_sensor_msgs.hpp>
#include <opencv2/core/types.hpp>

using namespace pointcloud_segmentation;
using namespace std::placeholders;
using namespace std::chrono;


ColorPointCloudNode::ColorPointCloudNode() : ColorPointCloudNode(rclcpp::NodeOptions()) {};

ColorPointCloudNode::ColorPointCloudNode(const rclcpp::NodeOptions& options)
: rclcpp::Node("color_point_cloud", options) {
  tf_buffer_ = std::make_unique<tf2_ros::Buffer>(this->get_clock());
  tf_listener_ = std::make_shared<tf2_ros::TransformListener>(*tf_buffer_);

  camera_info_sub_ = this->create_subscription<sensor_msgs::msg::CameraInfo>(
    "/camera/camera_info", 10, std::bind(&ColorPointCloudNode::camera_info_callback, this, _1));

  raw_image_sub_.subscribe(this, "/camera/image_raw", rclcpp::SensorDataQoS().get_rmw_qos_profile());
  raw_pc_sub_.subscribe(this, "/points", rclcpp::SensorDataQoS().get_rmw_qos_profile());
  synchronizer_ = std::make_unique<message_filters::Synchronizer<CameraLidarSyncPolicy>>(
    CameraLidarSyncPolicy(10), raw_image_sub_, raw_pc_sub_);
  synchronizer_->registerCallback(&ColorPointCloudNode::camera_lidar_callback, this);

  colored_pc_pub_ = this->create_publisher<sensor_msgs::msg::PointCloud2>("/colored_points", 10); 
}

ColorPointCloudNode::~ColorPointCloudNode() = default;

void ColorPointCloudNode::camera_info_callback(const sensor_msgs::msg::CameraInfo& camera_info_msg) {
  if (!got_camera_info_) {
    camera_model_.fromCameraInfo(camera_info_msg);
    got_camera_info_ = true;
  }
}

void ColorPointCloudNode::camera_lidar_callback(const sensor_msgs::msg::Image::SharedPtr raw_image,
                                                const sensor_msgs::msg::PointCloud2::SharedPtr raw_pc) {
  auto start_camera_lidar_callback = high_resolution_clock::now();

  // if we don't have camera_info_, we can't fuse 
  if (!got_camera_info_) return;

  // currently hardcoded
  if (raw_image->encoding != "rgb8" && raw_image->encoding != "bgr8") {
    RCLCPP_ERROR(this->get_logger(), "Image encoding must be \"rgb8\" or \"bgr8\"!");
    return;
  }

  const std::string& lidar_frame = raw_pc->header.frame_id;
  const std::string& camera_frame = camera_model_.tfFrame();


  geometry_msgs::msg::TransformStamped t;
  try {
    t = tf_buffer_->lookupTransform(camera_frame, lidar_frame,
                                    tf2::TimePointZero);
  } catch (const tf2::TransformException & ex) {
    RCLCPP_WARN(this->get_logger(), "Could not transform %s to %s: %s",
                lidar_frame.c_str(), camera_frame.c_str(), ex.what());
    return;
  }

  //sensor_msgs::msg::PointCloud2 tfed_pc;
  tf2::doTransform(*raw_pc, tfed_pc_, t); 

  // initialize new PointCloud2
  // see https://github.com/mikeferguson/ros2_cookbook/blob/main/rclcpp/pcl.md
  //sensor_msgs::msg::PointCloud2 out_pc_;
  out_pc_.header = raw_pc->header;

  out_pc_.height = raw_pc->height;
  out_pc_.width = raw_pc->width;

  sensor_msgs::PointCloud2Modifier mod(out_pc_);
  mod.setPointCloud2FieldsByString(2, "xyz", "rgb");
  mod.resize(out_pc_.width * out_pc_.height);


  sensor_msgs::PointCloud2ConstIterator<float> tfed_x_it(tfed_pc_, "x");
  sensor_msgs::PointCloud2ConstIterator<float> tfed_y_it(tfed_pc_, "y");
  sensor_msgs::PointCloud2ConstIterator<float> tfed_z_it(tfed_pc_, "z");

  sensor_msgs::PointCloud2ConstIterator<float> raw_x_it(*raw_pc, "x");
  sensor_msgs::PointCloud2ConstIterator<float> raw_y_it(*raw_pc, "y");
  sensor_msgs::PointCloud2ConstIterator<float> raw_z_it(*raw_pc, "z");

  sensor_msgs::PointCloud2Iterator<float> x_it(out_pc_, "x");
  sensor_msgs::PointCloud2Iterator<float> y_it(out_pc_, "y");
  sensor_msgs::PointCloud2Iterator<float> z_it(out_pc_, "z");
  sensor_msgs::PointCloud2Iterator<uint8_t> r_it(out_pc_, "r");
  sensor_msgs::PointCloud2Iterator<uint8_t> g_it(out_pc_, "g");
  sensor_msgs::PointCloud2Iterator<uint8_t> b_it(out_pc_, "b");



  cv::Size res = camera_model_.fullResolution();
  
  for (; raw_x_it != raw_x_it.end();
       ++tfed_x_it, ++tfed_y_it, ++tfed_z_it, ++raw_x_it, ++raw_y_it, ++raw_z_it,
       ++x_it, ++y_it, ++z_it, ++r_it, ++g_it, ++b_it) {
    // set xyz coords to original points
    *x_it = *raw_x_it;
    *y_it = *raw_y_it;
    *z_it = *raw_z_it;

    // use points tfed to camera frame for fusion
    float tfed_x = *tfed_x_it;
    float tfed_y = *tfed_y_it;
    float tfed_z = *tfed_z_it;
    cv::Point3d pt(tfed_x, tfed_y, tfed_z);
    // u = x, v = y
    cv::Point2d uv_rect = camera_model_.project3dToPixel(pt);
    cv::Point2d uv = camera_model_.unrectifyPoint(uv_rect);

    // filter out points that are outside the image frame or behind it
    if (uv.x >= 0 && uv.x < res.width && uv.y >= 0 && uv.y < res.height && tfed_z > 0) {
      // set pointcloud point rgb to rgb of pixel
      int32_t img_data_offset = get_8uc3_pixel_data_offset(uv.x, uv.y, raw_image->step);

      uint8_t* r, * b, * g;
      if (raw_image->encoding == "rgb8") {
        r = &raw_image->data[img_data_offset];
        g = r + 1;
        b = r + 2;
      } else if (raw_image->encoding == "bgr8") {
        b = &raw_image->data[img_data_offset];
        g = b + 1;
        r = b + 2;
      } else {
        RCLCPP_ERROR(this->get_logger(), "Something's wrong, you shouldn't be able to get here.");
        return;
      }

      uint8_t r_val = *r;
      uint8_t g_val = *g;
      uint8_t b_val = *b;
      *r_it = r_val;
      *g_it = g_val;
      *b_it = b_val;
    } else {
      // if not in our camera frame, set color to white
      *r_it = 255;
      *g_it = 255;
      *b_it = 255;
    }
  }

  colored_pc_pub_->publish(out_pc_);

  auto end_camera_lidar_callback = high_resolution_clock::now();
  auto camera_lidar_callback_duration = duration_cast<microseconds>(end_camera_lidar_callback -
                                                                    start_camera_lidar_callback);
  RCLCPP_INFO(this->get_logger(), "Colored pointcloud in %ld us", camera_lidar_callback_duration.count());
}
