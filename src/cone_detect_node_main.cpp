#include <memory>
#include <rclcpp/rclcpp.hpp>
#include "pointcloud_segmentation/cone_detect_node.hpp"

int main(int argc, char** argv) {
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<pointcloud_segmentation::ConeDetectNode>());
  rclcpp::shutdown();

  return 0;
}
