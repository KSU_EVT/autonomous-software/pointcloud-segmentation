#include <memory>
#include <rclcpp/rclcpp.hpp>
#include "pointcloud_segmentation/mask_cull_node.hpp"

int main(int argc, char** argv) {
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<pointcloud_segmentation::MaskCullNode>());
  rclcpp::shutdown();

  return 0;
}
