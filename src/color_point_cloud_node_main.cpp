#include <memory>
#include <rclcpp/rclcpp.hpp>
#include "pointcloud_segmentation/color_point_cloud_node.hpp"

int main(int argc, char** argv) {
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<pointcloud_segmentation::ColorPointCloudNode>());
  rclcpp::shutdown();
  return 0;
}
