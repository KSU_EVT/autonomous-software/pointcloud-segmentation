//#include <pointcloud_segmentation/pointcloud_segmentation_node.hpp>
//#include "sensor_msgs/msg/camera_info.hpp"
//
//#include "voltron_msgs/msg/box.hpp"
//#include "voltron_msgs/msg/cone_boxes.hpp"
//
//#include <cstdint>
//#include <cstddef>
//#include <memory>
//#include <rclcpp/logging.hpp>
//#include <rclcpp/qos.hpp>
//
//#include <pcl_conversions/pcl_conversions.h>
//#include <pcl/point_cloud.h>
//#include <pcl/point_types.h>
//#include <pcl/common/transforms.h>
//#include <tf2/transform_datatypes.h>
//#include <tf2_geometry_msgs/tf2_geometry_msgs.hpp>
//#include <tf2_ros/transform_listener.h>
//#include <tf2_ros/buffer.h>
//#include <tf2_ros/buffer_interface.h>
//#include <tf2_sensor_msgs/tf2_sensor_msgs.hpp>
//#include <tf2_eigen/tf2_eigen.h>
//#include <Eigen/Geometry>
//
//
//
//#include "message_filters/subscriber.h"
//#include "message_filters/time_synchronizer.h"
//#include "message_filters/sync_policies/approximate_time.h"
//
//using std::placeholders::_1;
//using std::placeholders::_2;
//using namespace message_filters;
//using namespace message_filters::sync_policies;
//
//namespace pointcloud_segmentation {  // TODO fixme Sahan :(
//
//typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::msg::PointCloud2, voltron_msgs::msg::ConeBoxes> MySyncPolicy;
//
//PointcloudSegmentationNode::PointcloudSegmentationNode() : PointcloudSegmentationNode(rclcpp::NodeOptions()) {};
//
//PointcloudSegmentationNode::PointcloudSegmentationNode(
//    const rclcpp::NodeOptions &options)
//    : rclcpp::Node("pointcloud_segmentation", options) {
//
//  camera_info_sub_ = this->create_subscription<sensor_msgs::msg::CameraInfo>(
//    "/track_seg_camera/camera_info", rclcpp::SensorDataQoS(),
//    std::bind(&PointcloudSegmentationNode::camera_info_callback, this, _1));
//
//  raw_pc_sub_ = create_subscription<sensor_msgs::msg::PointCloud2>(
//    "raw_pc",
//    rclcpp::QoS(10),
//    std::bind(&PointcloudSegmentationNode::raw_pc_callback, this, std::placeholders::_1));
//
//  cone_boxes_sub_ = std::make_shared<message_filters::Subscriber<voltron_msgs::msg::ConeBoxes>>(this, "cone_boxes");
//
//time_sync_ = std::make_shared<message_filters::Synchronizer<message_filters::sync_policies::ApproximateTime<sensor_msgs::msg::PointCloud2, voltron_msgs::msg::ConeBoxes>>>(MySyncPolicy(10), raw_pc_sub_, cone_boxes_sub_);
//  time_sync_->registerCallback(std::bind(&PointcloudSegmentationNode::synced_callback, this, _1, _2));
//
//  culled_pc_pub_ = this->create_publisher<sensor_msgs::msg::PointCloud2>("/segmented_points", rclcpp::SensorDataQoS());
//}
//
//void PointcloudSegmentationNode::synced_callback(const sensor_msgs::msg::PointCloud2::SharedPtr& pc_msg, const voltron_msgs::msg::ConeBoxes::SharedPtr& boxes_msg) {
//  raw_pc_ = *pc_msg;
//  raw_pc_received_ = true;
//
//  cones_boxes_msg_ = *boxes_msg;
//  cones_boxes_received_ = true;
//
//  pathFind();
//}
//
//void PointcloudSegmentationNode::pathFind() {
//  // Ensure you have received the PointCloud2 and CameraInfo messages
//  if (!raw_pc_received_ || !camera_info_received_)
//    return;
//
//  // Convert PointCloud2 message to a PCL point cloud
//  pcl::PointCloud<pcl::PointXYZ> pcl_cloud;
//  pcl::fromROSMsg(raw_pc_, pcl_cloud);
//
//  // // Create a tf2_ros::Buffer and a tf2_ros::TransformListener
//  // tf2_ros::Buffer tf_buffer;
//  // tf2_ros::TransformListener tf_listener((*tf_buffer).getBuffer(), shared_from_this(), false);
//
//  // Create a tf2_ros::Buffer and a tf2_ros::TransformListener
//  auto node = std::make_shared<rclcpp::Node>("pointcloud_segmentation_buffer_interface");
//  auto tf_buffer = std::make_shared<tf2_ros::Buffer>(node->get_clock());
//  auto tf_listener = std::make_shared<tf2_ros::TransformListener>(*tf_buffer);
//
//  // Get the transform from lidar sensor frame to camera frame
//  geometry_msgs::msg::TransformStamped transform;
//  try {
//    transform = tf_buffer->lookupTransform("camera_frame", "lidar_frame", rclcpp::Time(0)); // TODO make sure these frame names are valid
//  } catch (tf2::TransformException &ex) {
//    RCLCPP_ERROR(this->get_logger(), "%s", ex.what());
//    return;
//  }
//
//  // Apply the transform to the point cloud
//  pcl::PointCloud<pcl::PointXYZ> transformed_cloud;
//  Eigen::Affine3d eigen_transform = tf2::transformToEigen(transform.transform);
//  pcl::transformPointCloud(pcl_cloud, transformed_cloud, eigen_transform);
//
//
//  // Filter the point cloud based on bounding boxes
//  pcl::PointCloud<pcl::PointXYZ> filtered_cloud;
//  filterPointsByBoundingBoxes(transformed_cloud, filtered_cloud);
//
//  raw_pc_received_ = false; // Reset the flag
//
//  // Convert the filtered PCL point cloud to a PointCloud2 message
//  sensor_msgs::msg::PointCloud2 filtered_pc_msg;
//  pcl::toROSMsg(filtered_cloud, filtered_pc_msg);
//
//  // Set the header for the filtered point cloud message
//  filtered_pc_msg.header.stamp = raw_pc_.header.stamp;
//  filtered_pc_msg.header.frame_id = "camera_frame"; // TODO make sure this frame name is valid
//
//  // Publish the filtered point cloud message
//  culled_pc_pub_->publish(filtered_pc_msg);
//}
//
//// CameraInfo callback function
//void PointcloudSegmentationNode::camera_info_callback(const sensor_msgs::msg::CameraInfo::SharedPtr msg) {
//  camera_info_msg_ = *msg;
//  camera_info_received_ = true;
//}
//
//// PointCloud2 callback function
//void PointcloudSegmentationNode::raw_pc_callback(const std::shared_ptr<sensor_msgs::msg::PointCloud2> msg) {
//  raw_pc_ = *msg;
//  raw_pc_received_ = true;
//}
//
//// ConeBoxes callback function
//void PointcloudSegmentationNode::cone_boxes_callback(const voltron_msgs::msg::ConeBoxes::SharedPtr msg) {
//  cones_boxes_msg_ = *msg;
//}
//
//void PointcloudSegmentationNode::filterPointsByBoundingBoxes(const pcl::PointCloud<pcl::PointXYZ>& input_cloud, pcl::PointCloud<pcl::PointXYZ>& output_cloud) {
//  // Extract the camera intrinsic matrix from the CameraInfo message
//  float fx = camera_info_msg_.k[0]; // in pixel units
//  float fy = camera_info_msg_.k[4]; // in pixel units
//  float cx = camera_info_msg_.k[2];
//  float cy = camera_info_msg_.k[5];
//
//  // Project 3D points from the transformed point cloud onto the 2D image plane
//  for (const auto &point : input_cloud) {
//    // Skip the point if it is behind the camera (Z <= 0)
//    if (point.z <= 0)
//      continue;
//
//    // Compute the 2D image coordinates (u, v) for the 3D point (X, Y, Z)
//    float u = fx * point.x / point.z + cx;
//    float v = fy * point.y / point.z + cy;
//
//    // Check if the projected point is within any bounding box
//    bool point_in_box = false;
//    for (const auto &box : cones_boxes_msg_.boxes) {
//      if (u >= box.xmin && u <= box.xmax && v >= box.ymin && v <= box.ymax) {
//        point_in_box = true;
//        break;
//      }
//    }
//
//    // Add the point to the output_cloud if it lies within a bounding box
//    if (point_in_box) {
//      output_cloud.push_back(point);
//    }
//  }
//}
//
//PointcloudSegmentationNode::~PointcloudSegmentationNode() = default; // default destructor
//
//}  // namespace pointcloud_segmentation
