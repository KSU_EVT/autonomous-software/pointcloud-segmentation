#include "pointcloud_segmentation/cone_detect_node.hpp"

#include <Eigen/src/Geometry/Transform.h>
#include <pcl/PointIndices.h>
#include <pcl/filters/frustum_culling.h>
#include <pcl_conversions/pcl_conversions.h>
#include <tf2/convert.h>

#include <chrono>
#include <cmath>
#include <functional>
#include <geometry_msgs/msg/transform_stamped.hpp>
#include <limits>
#include <memory>
#include <opencv2/core/types.hpp>
#include <pcl/impl/point_types.hpp>
#include <pcl_ros/transforms.hpp>
#include <rclcpp/qos.hpp>
#include <rclcpp/subscription.hpp>
#include <tf2_eigen/tf2_eigen.hpp>
#include <tf2_sensor_msgs/tf2_sensor_msgs.hpp>

using namespace pointcloud_segmentation;
using namespace std::placeholders;
using namespace std::chrono;

static constexpr double rad2deg(double rad) { return rad * 180.0 / M_PI; }

ConeDetectNode::ConeDetectNode() : ConeDetectNode(rclcpp::NodeOptions()){};

ConeDetectNode::ConeDetectNode(const rclcpp::NodeOptions& options)
    : rclcpp::Node("cone_detect", options) {
  negative_ = this->declare_parameter<bool>("negative", false);

  tf_buffer_ = std::make_unique<tf2_ros::Buffer>(this->get_clock());
  tf_listener_ = std::make_shared<tf2_ros::TransformListener>(*tf_buffer_);

  camera_info_sub_ = this->create_subscription<sensor_msgs::msg::CameraInfo>(
      "/camera/camera_info", 10,
      std::bind(&ConeDetectNode::camera_info_callback, this, _1));

  cone_boxes_sub_.subscribe(this, "/cone_boxes");
  raw_pc_sub_.subscribe(this, "/points",
                        rclcpp::SensorDataQoS().get_rmw_qos_profile());
  synchronizer_ =
      std::make_unique<message_filters::Synchronizer<CameraLidarSyncPolicy>>(
          CameraLidarSyncPolicy(100), cone_boxes_sub_, raw_pc_sub_);
  synchronizer_->registerCallback(&ConeDetectNode::camera_lidar_callback, this);

  cones_pc_pub_ = this->create_publisher<sensor_msgs::msg::PointCloud2>(
      "/cone_points", 10);

  non_tfed_pc_ = std::make_shared<pcl::PointCloud<pcl::PointXYZ>>();
  frustum_culled_pc_ = std::make_shared<pcl::PointCloud<pcl::PointXYZ>>();
  tfed_frustum_culled_pc_ = std::make_shared<pcl::PointCloud<pcl::PointXYZ>>();
  cones_pc_ = std::make_shared<pcl::PointCloud<pcl::PointXYZ>>();

  average_points_dist_x_ = this->declare_parameter<double>("average_points_dist_x");
  average_points_dist_y_ = this->declare_parameter<double>("average_points_dist_y");
  average_points_dist_z_ = this->declare_parameter<double>("average_points_dist_z");

  enable_debug_topics_ = this->declare_parameter<bool>("enable_debug_topics", false);
  if (enable_debug_topics_) {
    debug_bbox_points_pc_pub_ =  this->create_publisher<sensor_msgs::msg::PointCloud2>("/debug_bbox_points", 10);
    debug_bbox_points_pc_ = std::make_shared<pcl::PointCloud<pcl::PointXYZ>>();
  }
}

ConeDetectNode::~ConeDetectNode() = default;

void ConeDetectNode::camera_info_callback(
    const sensor_msgs::msg::CameraInfo& camera_info_msg) {
  if (!got_camera_info_) {
    camera_model_.fromCameraInfo(camera_info_msg);
    got_camera_info_ = true;
  }
}

void ConeDetectNode::camera_lidar_callback(
    const voltron_msgs::msg::ConeBoxes::SharedPtr cone_boxes,
    const sensor_msgs::msg::PointCloud2::SharedPtr raw_pc_msg) {
  // auto start_camera_lidar_callback = high_resolution_clock::now();

  if (!got_lidar_camera_tf_) {
    const std::string& lidar_frame = raw_pc_msg->header.frame_id;
    const std::string& camera_frame = camera_model_.tfFrame();
    try {
      lidar_camera_tf_ = tf_buffer_->lookupTransform(camera_frame, lidar_frame,
                                                     tf2::TimePointZero);

      auto camera_tf = tf2::transformToEigen(lidar_camera_tf_);
      camera_tf.matrix() = camera_tf.matrix().inverse().eval();
      // Switch to PCL (minecraft) coords (X is forward, Y is up, Z is right)
      Eigen::Matrix4d cam2minecraft;
      cam2minecraft << 0, 0, 1, 0,
                       0,-1, 0, 0,
                       1, 0, 0, 0,
                       0, 0, 0, 1;
      camera_tf.matrix() = camera_tf.matrix() * cam2minecraft;

      camera_pose_ = (camera_tf.matrix()).cast<float>();

      got_lidar_camera_tf_ = true;
    } catch (const tf2::TransformException& ex) {
      RCLCPP_WARN(this->get_logger(), "Could not transform %s to %s: %s",
                  lidar_frame.c_str(), camera_frame.c_str(), ex.what());
      return;
    }
  }

  RCLCPP_INFO(this->get_logger(), "=============");

  //  if we don't have camera_info_, we can't fuse
  if (!got_camera_info_) return;

  //auto start_pcl = high_resolution_clock::now();
  pcl::fromROSMsg(*raw_pc_msg, *non_tfed_pc_);
  //auto end_pcl = high_resolution_clock::now();
  //auto pcl_duration = duration_cast<microseconds>(end_pcl - start_pcl);

  //RCLCPP_INFO(this->get_logger(), "converted to pcl in %ld us",
  //            pcl_duration.count());

  //RCLCPP_INFO(this->get_logger(), "size of pointcloud: %lu ; dense: %d",
  //            non_tfed_pc_->size(), non_tfed_pc_->is_dense);

  //auto start_frustum = high_resolution_clock::now();
  pcl::FrustumCulling<pcl::PointXYZ> fc;
  fc.setHorizontalFOV(rad2deg(camera_model_.fovX()));
  fc.setVerticalFOV(rad2deg(camera_model_.fovY()));

  fc.setCameraPose(camera_pose_);

  fc.setNearPlaneDistance(0.1);
  // setting far plane dist to std::numeric_limits<float>::max() as per the docs makes it filter out everything
  fc.setFarPlaneDistance(999.0);

  fc.setInputCloud(non_tfed_pc_);
  fc.filter(*frustum_culled_pc_);
  //auto frustum_duration =
  //    duration_cast<microseconds>(high_resolution_clock::now() - start_frustum);
  //RCLCPP_INFO(this->get_logger(), "frustum culled in %ld us",
  //             frustum_duration.count());
  //RCLCPP_INFO(this->get_logger(), "size of culled: %lu",
  //            frustum_culled_pc_->size());


  //auto start_tf = high_resolution_clock::now();

  pcl_ros::transformPointCloud(*frustum_culled_pc_, *tfed_frustum_culled_pc_, lidar_camera_tf_);
  //auto end_tf = high_resolution_clock::now();
  //auto tf_duration = duration_cast<microseconds>(end_tf - start_tf);
  //RCLCPP_INFO(this->get_logger(), "tfed in %ld us", tf_duration.count());


  //auto start_yeet = high_resolution_clock::now();
  if (enable_debug_topics_) {
    debug_bbox_points_pc_->points.resize(0);
  }

  // reset the cones / points table only allocating memory if the new size is greater than before
  cones_points_.resize(cone_boxes->boxes.size(), std::vector<std::pair<pcl::PointXYZ, pcl::PointXYZ>>());
  for (auto& points_vec : cones_points_) {
    points_vec.resize(0);
  }

  for (size_t i = 0; i < tfed_frustum_culled_pc_->points.size(); i++) {
    const auto& p = tfed_frustum_culled_pc_->points[i];
    const auto& non_tfed_p = frustum_culled_pc_->points[i];

    if (!(std::isfinite(p.x) && std::isfinite(p.y) && std::isfinite(p.z))) {
      RCLCPP_WARN(this->get_logger(), "got nan or inf value");
      continue;
    }

    cv::Point3d pt(p.x, p.y, p.z);
  
    cv::Point2d uv_rect = camera_model_.project3dToPixel(pt);
    cv::Rect roi = camera_model_.rawRoi();
    if (!(roi.x < uv_rect.x && uv_rect.x < (roi.x + roi.width) &&
          roi.y < uv_rect.y && uv_rect.y < (roi.y + roi.height))) {
      continue;
    }

    cv::Point2d uv = camera_model_.unrectifyPoint(uv_rect);

    for (size_t cone_idx = 0; cone_idx < cone_boxes->boxes.size(); cone_idx++) {
      const auto& bbox = cone_boxes->boxes[cone_idx];

      if (uv.x >= bbox.xmin && uv.x < bbox.xmax && uv.y >= bbox.ymin && uv.y < bbox.ymax) {
        cones_points_[cone_idx].push_back({p, non_tfed_p});

        if (enable_debug_topics_) {
          debug_bbox_points_pc_->points.push_back(non_tfed_p);
        }
      }
    }
  }

  if (enable_debug_topics_) {
    pcl::toROSMsg(*debug_bbox_points_pc_, debug_bbox_points_pc_msg_);
    debug_bbox_points_pc_msg_.header = raw_pc_msg->header;
    debug_bbox_points_pc_pub_->publish(debug_bbox_points_pc_msg_);
  }

  // reset cones_pc_ 
  cones_pc_->points.resize(0);

  // for each detection, get the average of every point within N cm xyz of the nearest point
  for (const auto& single_det_points : cones_points_) {
    if (single_det_points.empty()) continue; // don't segfault if there's a bbox without any points in it

    double min_range = 99999.0;
    size_t min_range_idx = 0;

    for (size_t i = 0; i < single_det_points.size(); i++) {
      const auto& p = single_det_points[i].first;

      double range = std::hypot(p.x, p.y, p.z);
      if (range < min_range) {
        min_range_idx = i;
        min_range = range;
      }
    }

    // in lidar frame (not tfed)
    const auto& nearest_p = single_det_points[min_range_idx].second;

    double sum_x = nearest_p.x;
    double sum_y = nearest_p.y;
    double sum_z = nearest_p.z;
    int count = 1;
    for (size_t i = 0; i < single_det_points.size(); i++) {
      const auto& p = single_det_points[i].second;
      if (std::fabs(nearest_p.x - p.x) <= average_points_dist_x_ &&
          std::fabs(nearest_p.y - p.y) <= average_points_dist_y_ &&
          std::fabs(nearest_p.z - p.z) <= average_points_dist_z_) {
        sum_x += p.x;
        sum_y += p.y;
        sum_z += p.z;
        count++;
      }
    }

    const double avg_x = sum_x / (double)count;
    const double avg_y = sum_y / (double)count;
    const double avg_z = sum_z / (double)count;

    // one point per detected cone
    cones_pc_->points.emplace_back(avg_x, avg_y, avg_z);
  }
  
  //auto end_yeet = high_resolution_clock::now();
  //auto yeet_duration = duration_cast<microseconds>(end_yeet - start_yeet);
  //RCLCPP_INFO(this->get_logger(), "yeeted in %ld us", yeet_duration.count());

  pcl::toROSMsg(*cones_pc_, cones_pc_msg_);
  cones_pc_msg_.header = raw_pc_msg->header;
  cones_pc_pub_->publish(cones_pc_msg_);

}
