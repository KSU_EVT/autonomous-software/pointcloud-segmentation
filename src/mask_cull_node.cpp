#include "pointcloud_segmentation/mask_cull_node.hpp"

#include <Eigen/src/Geometry/Transform.h>
#include <pcl/PointIndices.h>
#include <pcl/filters/frustum_culling.h>
#include <pcl_conversions/pcl_conversions.h>
#include <tf2/convert.h>

#include <chrono>
#include <cmath>
#include <functional>
#include <geometry_msgs/msg/detail/pose_stamped__struct.hpp>
#include <geometry_msgs/msg/detail/transform_stamped__struct.hpp>
#include <geometry_msgs/msg/transform_stamped.hpp>
#include <limits>
#include <memory>
#include <opencv2/core/types.hpp>
#include <pcl/impl/point_types.hpp>
#include <pcl_ros/transforms.hpp>
#include <rclcpp/qos.hpp>
#include <rclcpp/subscription.hpp>
#include <sensor_msgs/point_cloud2_iterator.hpp>
#include <tf2_eigen/tf2_eigen.hpp>
#include <tf2_sensor_msgs/tf2_sensor_msgs.hpp>

using namespace pointcloud_segmentation;
using namespace std::placeholders;
using namespace std::chrono;

static constexpr double rad2deg(double rad) { return rad * 180.0 / M_PI; }

MaskCullNode::MaskCullNode() : MaskCullNode(rclcpp::NodeOptions()){};

MaskCullNode::MaskCullNode(const rclcpp::NodeOptions& options)
    : rclcpp::Node("mask_cull", options) {
  negative_ = this->declare_parameter<bool>("negative", false);

  tf_buffer_ = std::make_unique<tf2_ros::Buffer>(this->get_clock());
  tf_listener_ = std::make_shared<tf2_ros::TransformListener>(*tf_buffer_);

  camera_info_sub_ = this->create_subscription<sensor_msgs::msg::CameraInfo>(
      "/camera/camera_info", 10,
      std::bind(&MaskCullNode::camera_info_callback, this, _1));

  raw_image_sub_.subscribe(this, "/camera/seg",
                           rclcpp::SensorDataQoS().get_rmw_qos_profile());
  raw_pc_sub_.subscribe(this, "/points",
                        rclcpp::SensorDataQoS().get_rmw_qos_profile());
  synchronizer_ =
      std::make_unique<message_filters::Synchronizer<CameraLidarSyncPolicy>>(
          CameraLidarSyncPolicy(10), raw_image_sub_, raw_pc_sub_);
  synchronizer_->registerCallback(&MaskCullNode::camera_lidar_callback, this);

  colored_pc_pub_ = this->create_publisher<sensor_msgs::msg::PointCloud2>(
      "/mask_culled_points", 10);

  non_tfed_pc_ = std::make_shared<pcl::PointCloud<pcl::PointXYZ>>();
  frustum_culled_pc_ = std::make_shared<pcl::PointCloud<pcl::PointXYZ>>();
  tfed_frustum_culled_pc_ = std::make_shared<pcl::PointCloud<pcl::PointXYZ>>();
  masked_pc_ = std::make_shared<pcl::PointCloud<pcl::PointXYZ>>();
}

MaskCullNode::~MaskCullNode() = default;

void MaskCullNode::camera_info_callback(
    const sensor_msgs::msg::CameraInfo& camera_info_msg) {
  if (!got_camera_info_) {
    camera_model_.fromCameraInfo(camera_info_msg);
    got_camera_info_ = true;
  }
}

void MaskCullNode::camera_lidar_callback(
    const sensor_msgs::msg::Image::SharedPtr raw_image,
    const sensor_msgs::msg::PointCloud2::SharedPtr raw_pc) {
  // auto start_camera_lidar_callback = high_resolution_clock::now();

  if (!got_lidar_camera_tf_) {
    const std::string& lidar_frame = raw_pc->header.frame_id;
    const std::string& camera_frame = camera_model_.tfFrame();
    try {
      lidar_camera_tf_ = tf_buffer_->lookupTransform(camera_frame, lidar_frame,
                                                     tf2::TimePointZero);

      auto camera_tf = tf2::transformToEigen(lidar_camera_tf_);
      camera_tf.matrix() = camera_tf.matrix().inverse().eval();
      // Switch to PCL (minecraft) coords (X is forward, Y is up, Z is right)
      Eigen::Matrix4d cam2minecraft;
      cam2minecraft << 0, 0, 1, 0,
                       0,-1, 0, 0,
                       1, 0, 0, 0,
                       0, 0, 0, 1;
      camera_tf.matrix() = camera_tf.matrix() * cam2minecraft;

      camera_pose_ = (camera_tf.matrix()).cast<float>();

      got_lidar_camera_tf_ = true;
    } catch (const tf2::TransformException& ex) {
      RCLCPP_WARN(this->get_logger(), "Could not transform %s to %s: %s",
                  lidar_frame.c_str(), camera_frame.c_str(), ex.what());
      return;
    }
  }

  RCLCPP_INFO(this->get_logger(), "=============");

  //  if we don't have camera_info_, we can't fuse
  if (!got_camera_info_) return;

  // currently hardcoded
  if (raw_image->encoding != "rgb8" && raw_image->encoding != "bgr8") {
    RCLCPP_ERROR(this->get_logger(),
                 "Image encoding must be \"rgb8\" or \"bgr8\"!");
    return;
  }

  auto start_pcl = high_resolution_clock::now();
  pcl::fromROSMsg(*raw_pc, *non_tfed_pc_);
  auto end_pcl = high_resolution_clock::now();
  auto pcl_duration = duration_cast<microseconds>(end_pcl - start_pcl);

  RCLCPP_INFO(this->get_logger(), "converted to pcl in %ld us",
              pcl_duration.count());

  RCLCPP_INFO(this->get_logger(), "size of pointcloud: %lu ; dense: %d",
              non_tfed_pc_->size(), non_tfed_pc_->is_dense);

  auto start_frustum = high_resolution_clock::now();
  pcl::FrustumCulling<pcl::PointXYZ> fc;
  fc.setHorizontalFOV(rad2deg(camera_model_.fovX()));
  fc.setVerticalFOV(rad2deg(camera_model_.fovY()));

  fc.setCameraPose(camera_pose_);

  fc.setNearPlaneDistance(0.1);
  // setting far plane dist to std::numeric_limits<float>::max() as per the docs makes it filter out everything
  fc.setFarPlaneDistance(999.0);

  fc.setInputCloud(non_tfed_pc_);
  fc.filter(*frustum_culled_pc_);
  auto frustum_duration =
      duration_cast<microseconds>(high_resolution_clock::now() - start_frustum);
  RCLCPP_INFO(this->get_logger(), "frustum culled in %ld us",
               frustum_duration.count());
  RCLCPP_INFO(this->get_logger(), "size of culled: %lu",
              frustum_culled_pc_->size());

  // RCLCPP_INFO(this->get_logger(), "=============");

  auto start_tf = high_resolution_clock::now();
  pcl_ros::transformPointCloud(*frustum_culled_pc_, *tfed_frustum_culled_pc_, lidar_camera_tf_);
  auto end_tf = high_resolution_clock::now();
  auto tf_duration = duration_cast<microseconds>(end_tf - start_tf);
  RCLCPP_INFO(this->get_logger(), "tfed in %ld us", tf_duration.count());


  auto start_yeet = high_resolution_clock::now();
  cv::Size res = camera_model_.fullResolution();

  masked_pc_->points.resize(0);
  for (size_t i = 0; i < tfed_frustum_culled_pc_->points.size(); i++) {
    const auto& p = tfed_frustum_culled_pc_->points[i];
    const auto& non_tfed_p = frustum_culled_pc_->points[i];

    if (!(std::isfinite(p.x) && std::isfinite(p.y) && std::isfinite(p.z))) {
      RCLCPP_WARN(this->get_logger(), "got nan or inf value");
      continue;
    }


    cv::Point3d pt(p.x, p.y, p.z);
  
    cv::Point2d uv_rect = camera_model_.project3dToPixel(pt);
    cv::Point2d uv = camera_model_.unrectifyPoint(uv_rect);

    if (uv.x >= 0 && uv.x < res.width && uv.y >= 0 && uv.y < res.height) {
      int32_t img_data_offset = get_8uc3_pixel_data_offset(uv.x, uv.y, raw_image->step);

      uint8_t* r, * b, * g;
      if (raw_image->encoding == "rgb8") {
        r = &raw_image->data[img_data_offset];
        g = r + 1;
        b = r + 2;
      } else if (raw_image->encoding == "bgr8") {
        b = &raw_image->data[img_data_offset];
        g = b + 1;
        r = b + 2;
      } else {
        RCLCPP_ERROR(this->get_logger(), "Something's wrong, you shouldn't be able to get here.");
        return;
      }
      uint32_t rgb = (*r << 16) + (*g << 8) + *b;

      // if not black, we have a masky boi
      if (rgb != 0x000000) {
        masked_pc_->points.push_back(non_tfed_p);
      }
    }
  }
  auto end_yeet = high_resolution_clock::now();
  auto yeet_duration = duration_cast<microseconds>(end_yeet - start_yeet);
  RCLCPP_INFO(this->get_logger(), "yeeted in %ld us", yeet_duration.count());

  pcl::toROSMsg(*masked_pc_, masked_pc_msg_);
  masked_pc_msg_.header = raw_pc->header;
  colored_pc_pub_->publish(masked_pc_msg_);

}
