#pragma once

#include <memory>
#include <mutex>
#include <queue>
#include <rclcpp/node.hpp>
#include <rclcpp/node_options.hpp>
#include <sensor_msgs/msg/point_cloud2.hpp>
#include <sensor_msgs/msg/camera_info.hpp>

#include "voltron_msgs/msg/box.hpp"
#include "voltron_msgs/msg/cone_boxes.hpp"

#include "message_filters/subscriber.h"
#include "message_filters/synchronizer.h"
#include "message_filters/sync_policies/approximate_time.h"

#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/common/transforms.h>
#include <tf2/transform_datatypes.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.hpp>
#include <tf2_ros/transform_listener.h>
#include <tf2_ros/buffer.h>
#include <tf2_sensor_msgs/tf2_sensor_msgs.hpp>

namespace pointcloud_segmentation {

//class PointcloudSegmentationNode : public rclcpp::Node
//{
//private:
//  std::shared_ptr<rclcpp::Subscription<sensor_msgs::msg::CameraInfo>> camera_info_sub_;
//  std::shared_ptr<rclcpp::Subscription<sensor_msgs::msg::PointCloud2>> raw_pc_sub_;
//  std::shared_ptr<message_filters::Subscriber<voltron_msgs::msg::ConeBoxes>> cone_boxes_sub_;
//
//  std::shared_ptr<message_filters::Synchronizer<message_filters::sync_policies::ApproximateTime<sensor_msgs::msg::PointCloud2, voltron_msgs::msg::ConeBoxes>>> time_sync_;
//
//  typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::msg::PointCloud2, voltron_msgs::msg::ConeBoxes> MySyncPolicy;
//
//  std::shared_ptr<rclcpp::Publisher<sensor_msgs::msg::PointCloud2>> culled_pc_pub_;
//
//  void synced_callback(const sensor_msgs::msg::PointCloud2::SharedPtr& pc_msg, const voltron_msgs::msg::ConeBoxes::SharedPtr& boxes_msg);
//
//  // Member variables
//  bool camera_info_received_ = false;
//  bool raw_pc_received_ = false;
//  bool cones_boxes_received_ = false;
//  sensor_msgs::msg::CameraInfo camera_info_msg_;
//  sensor_msgs::msg::PointCloud2 raw_pc_;
//  voltron_msgs::msg::ConeBoxes cones_boxes_msg_;
//
//  // Callback functions
//  void camera_info_callback(const sensor_msgs::msg::CameraInfo::SharedPtr msg);
//  void raw_pc_callback(const sensor_msgs::msg::PointCloud2::SharedPtr msg);
//  void cone_boxes_callback(const voltron_msgs::msg::ConeBoxes::SharedPtr msg);
//
//  // Member functions
//  void pathFind();
//  void filterPointsByBoundingBoxes(const pcl::PointCloud<pcl::PointXYZ>& input_cloud, pcl::PointCloud<pcl::PointXYZ>& output_cloud);
//
//
//public:
//  PointcloudSegmentationNode();
//  explicit PointcloudSegmentationNode(const rclcpp::NodeOptions &options);
//  ~PointcloudSegmentationNode();
//};

} // namespace pointcloud_segmentation
