#pragma once

#include <geometry_msgs/msg/detail/pose_stamped__struct.hpp>
#include <geometry_msgs/msg/detail/transform_stamped__struct.hpp>
#include <memory>
#include <mutex>
#include <queue>
#include <rclcpp/node.hpp>
#include <rclcpp/node_options.hpp>
#include <sensor_msgs/msg/point_cloud2.hpp>
#include <sensor_msgs/msg/camera_info.hpp>
#include <sensor_msgs/msg/image.hpp>
#include <geometry_msgs/msg/pose_stamped.hpp>

#include "message_filters/subscriber.h"
#include "message_filters/synchronizer.h"
#include "message_filters/sync_policies/approximate_time.h"

#include <Eigen/Dense>
#include <pcl/PointIndices.h>
#include <tf2/exceptions.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2_ros/buffer.h>

#include <image_geometry/pinhole_camera_model.hpp>

#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

namespace pointcloud_segmentation {

class MaskCullNode : public rclcpp::Node {
private:
  typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::msg::Image,
                                                          sensor_msgs::msg::PointCloud2> CameraLidarSyncPolicy;

  bool negative_;

  std::shared_ptr<tf2_ros::TransformListener> tf_listener_{nullptr};
  std::unique_ptr<tf2_ros::Buffer> tf_buffer_;

  rclcpp::Subscription<sensor_msgs::msg::CameraInfo>::SharedPtr camera_info_sub_;

  message_filters::Subscriber<sensor_msgs::msg::Image> raw_image_sub_;
  message_filters::Subscriber<sensor_msgs::msg::PointCloud2> raw_pc_sub_;
  std::unique_ptr<message_filters::Synchronizer<CameraLidarSyncPolicy>> synchronizer_;

  rclcpp::Publisher<sensor_msgs::msg::PointCloud2>::SharedPtr colored_pc_pub_;

  bool got_camera_info_ = false;
  image_geometry::PinholeCameraModel camera_model_;
  Eigen::Matrix4f camera_pose_; // camera pose in lidar frame

  bool got_lidar_camera_tf_ = false;
  geometry_msgs::msg::TransformStamped lidar_camera_tf_;

  // Members only to save on allocations:
  pcl::PointCloud<pcl::PointXYZ>::Ptr non_tfed_pc_;
  pcl::PointCloud<pcl::PointXYZ>::Ptr frustum_culled_pc_;
  pcl::PointCloud<pcl::PointXYZ>::Ptr tfed_frustum_culled_pc_;
  pcl::PointCloud<pcl::PointXYZ>::Ptr masked_pc_;
  sensor_msgs::msg::PointCloud2 masked_pc_msg_;

  void camera_info_callback(const sensor_msgs::msg::CameraInfo& camera_info_msg);

  void camera_lidar_callback(const sensor_msgs::msg::Image::SharedPtr raw_image,
                             const sensor_msgs::msg::PointCloud2::SharedPtr raw_pc);

  inline static int32_t get_8uc3_pixel_data_offset(const int32_t u, const int32_t v, const int32_t step) {
    return (v*step + u*3); // we have 3 byte pixels. offset is to the beginning byte of a pixel
  }

public:
  explicit MaskCullNode();
  explicit MaskCullNode(const rclcpp::NodeOptions& options);
  ~MaskCullNode();
};

} // namespace pointcloud_segmentation
