#pragma once

#include <memory>
#include <mutex>
#include <queue>
#include <rclcpp/node.hpp>
#include <rclcpp/node_options.hpp>
#include <sensor_msgs/msg/point_cloud2.hpp>
#include <sensor_msgs/msg/camera_info.hpp>
#include <sensor_msgs/msg/image.hpp>

#include "message_filters/subscriber.h"
#include "message_filters/synchronizer.h"
#include "message_filters/sync_policies/approximate_time.h"

#include <tf2/exceptions.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_ros/buffer.h>

#include <image_geometry/pinhole_camera_model.hpp>

//#include <pcl_conversions/pcl_conversions.h>
//#include <pcl/point_cloud.h>
//#include <pcl/point_types.h>
//#include <pcl/common/transforms.h>
//#include <tf2/transform_datatypes.h>
//#include <tf2_geometry_msgs/tf2_geometry_msgs.hpp>
//#include <tf2_ros/transform_listener.h>
//#include <tf2_ros/buffer.h>
//#include <tf2_sensor_msgs/tf2_sensor_msgs.hpp>

namespace pointcloud_segmentation {

class ColorPointCloudNode : public rclcpp::Node {
private:
  typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::msg::Image,
                                                          sensor_msgs::msg::PointCloud2> CameraLidarSyncPolicy;


  std::shared_ptr<tf2_ros::TransformListener> tf_listener_{nullptr};
  std::unique_ptr<tf2_ros::Buffer> tf_buffer_;

  rclcpp::Subscription<sensor_msgs::msg::CameraInfo>::SharedPtr camera_info_sub_;

  message_filters::Subscriber<sensor_msgs::msg::Image> raw_image_sub_;
  message_filters::Subscriber<sensor_msgs::msg::PointCloud2> raw_pc_sub_;
  std::unique_ptr<message_filters::Synchronizer<CameraLidarSyncPolicy>> synchronizer_;

  rclcpp::Publisher<sensor_msgs::msg::PointCloud2>::SharedPtr colored_pc_pub_;


  bool got_camera_info_ = false;
  image_geometry::PinholeCameraModel camera_model_;

  void camera_info_callback(const sensor_msgs::msg::CameraInfo& camera_info_msg);

  void camera_lidar_callback(const sensor_msgs::msg::Image::SharedPtr raw_image,
                             const sensor_msgs::msg::PointCloud2::SharedPtr raw_pc);

  inline static int32_t get_8uc3_pixel_data_offset(const int32_t u, const int32_t v, const int32_t step) {
    return (v*step + u*3); // we have 3 byte pixels. offset is to the beginning byte of a pixel
  }

  // members just for saving allocations
  sensor_msgs::msg::PointCloud2 tfed_pc_;
  sensor_msgs::msg::PointCloud2 out_pc_;

public:
  explicit ColorPointCloudNode();
  explicit ColorPointCloudNode(const rclcpp::NodeOptions& options);
  ~ColorPointCloudNode();
};

//class PointcloudSegmentationNode : public rclcpp::Node
//{
//private:
//  std::shared_ptr<rclcpp::Subscription<sensor_msgs::msg::CameraInfo>> camera_info_sub_;
//  std::shared_ptr<rclcpp::Subscription<sensor_msgs::msg::PointCloud2>> raw_pc_sub_;
//  std::shared_ptr<message_filters::Subscriber<voltron_msgs::msg::ConeBoxes>> cone_boxes_sub_;
//
//  std::shared_ptr<message_filters::Synchronizer<message_filters::sync_policies::ApproximateTime<sensor_msgs::msg::PointCloud2, voltron_msgs::msg::ConeBoxes>>> time_sync_;
//
//  typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::msg::PointCloud2, voltron_msgs::msg::ConeBoxes> MySyncPolicy;
//
//  std::shared_ptr<rclcpp::Publisher<sensor_msgs::msg::PointCloud2>> culled_pc_pub_;
//
//  void synced_callback(const sensor_msgs::msg::PointCloud2::SharedPtr& pc_msg, const voltron_msgs::msg::ConeBoxes::SharedPtr& boxes_msg);
//
//  // Member variables
//  bool camera_info_received_ = false;
//  bool raw_pc_received_ = false;
//  bool cones_boxes_received_ = false;
//  sensor_msgs::msg::CameraInfo camera_info_msg_;
//  sensor_msgs::msg::PointCloud2 raw_pc_;
//  voltron_msgs::msg::ConeBoxes cones_boxes_msg_;
//
//  // Callback functions
//  void camera_info_callback(const sensor_msgs::msg::CameraInfo::SharedPtr msg);
//  void raw_pc_callback(const sensor_msgs::msg::PointCloud2::SharedPtr msg);
//  void cone_boxes_callback(const voltron_msgs::msg::ConeBoxes::SharedPtr msg);
//
//  // Member functions
//  void pathFind();
//  void filterPointsByBoundingBoxes(const pcl::PointCloud<pcl::PointXYZ>& input_cloud, pcl::PointCloud<pcl::PointXYZ>& output_cloud);
//
//
//public:
//  PointcloudSegmentationNode();
//  explicit PointcloudSegmentationNode(const rclcpp::NodeOptions &options);
//  ~PointcloudSegmentationNode();
//};

} // namespace pointcloud_segmentation
