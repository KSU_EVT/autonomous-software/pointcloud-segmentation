# Pointcloud Segmentation

## Rationale

Currently, our kart lacks vision data which is needed to determine its reaction for any stimulus at an instant in time

This repository aims to take in a raw pointcloud from our LIDAR sensor, and will use a CameraInfo node to determine the relationship between the camera image plane (according to camera intrinsics according to the pinhole camera model) and the LIDAR points.

From there, this node will take in which pixels are within any cone bounding box generated from image object detection. For every LIDAR point, if it is not in a cone bounding box, it is removed.

The end result will be a pointcloud with a path cleared representing the track, and the points themselve represent cone obstacles for which the reactive controller will plan to avoid.